import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CharacterService } from '../../services/character/character.service';
import { FormBuilder, FormGroup } from "@angular/forms";
import { Validators } from "@angular/forms";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  searchBox: FormGroup;
  @Output() results = new EventEmitter<any>();
  
  constructor(
    private _character: CharacterService,
    private fb: FormBuilder,) {
    this.searchBox = this.fb.group({
      searchText: [null, Validators.required]
    });
   }

  ngOnInit(): void {
    this.searchText("Spider-Man");
    
  }

  searchText(text: string) {
    this._character.getCharacter(text).subscribe(data => {
      this.results.emit(data);
    });
  }
}
