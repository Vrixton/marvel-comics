import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FavoritesService } from 'src/app/services/favorites/favorites.service';

@Component({
  selector: 'app-favorite-card',
  templateUrl: './favorite-card.component.html',
  styleUrls: ['./favorite-card.component.scss']
})
export class FavoriteCardComponent implements OnInit {
  @Input() item: any;
  @Output() update = new EventEmitter<any>();
  
  constructor(private _favorite: FavoritesService) { }

  ngOnInit(): void {
  }

  removeFavorite(id: string) {
    this._favorite.removeFavorite(id);
    this.update.emit();
  }

}
