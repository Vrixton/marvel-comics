import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FavoritesService } from 'src/app/services/favorites/favorites.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  @Input() item: any;
  @Input() type: any;
  @Output() close = new EventEmitter<any>();
  @Output() update = new EventEmitter<any>();
  isFavorite: boolean = false;

  constructor(private _favorite : FavoritesService) { }

  ngOnInit(): void {
    console.log("modal --> ", this.item );
    this.isFavorite = this.existComic(this.item.id);
  }

  closeModal() {
    this.close.emit();
  }

  saveFavorite(item: Object) {
    this._favorite.saveFavorite(item);
    this.isFavorite = true;
    this.update.emit();
  }

  removeFavorite(id: string) {
    this._favorite.removeFavorite(id);
    this.isFavorite = false;
    this.update.emit();
  }

  existComic(id: string) {
    return this._favorite.existComic(id);
  }
}
