import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class FavoritesService {

  constructor(
    private http: HttpClient) { }

  saveFavorite(item: any) {
    const favorites = this.getFavorites();
    if(favorites) {
      favorites.push({
        title: item?.title,
        thumbnail: item?.thumbnail,
        id: item?.id
      });
      localStorage.setItem('favorites', JSON.stringify( favorites));
    }
    else {
      localStorage.setItem('favorites', JSON.stringify( [{
        title: item.title,
        thumbnail: item.thumbnail,
        id: item.id
      }] ));
    }
  }

  removeFavorite(id: string) {
    const favorites = this.getFavorites();
    const indexId = favorites.findIndex((element: any) => element.id === id);
    favorites.splice(indexId, 1);
    localStorage.setItem('favorites', JSON.stringify( favorites));
  }

  getFavorites() {
    const favorites = localStorage.getItem('favorites');
    if(favorites) {
      const favoriteList = JSON.parse(favorites);
      return favoriteList;
    }
    else {
      return false;
    }
  }

  existComic(id: string) {
    const favorites = this.getFavorites();
    const found = favorites.find((element: any) => element.id === id );
    return found ? true : false;
  }

}
